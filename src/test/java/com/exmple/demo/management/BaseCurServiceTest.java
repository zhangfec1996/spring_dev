package com.exmple.demo.management;

import cn.hutool.json.JSONUtil;
import com.example.demo.DemoApplication;
import com.example.demo.management.service.BaseCurService;
import com.example.demo.util.JgitUtils;
import com.example.demo.vo.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.*;

/**
 * @author zhangfc
 * @date 2021/12/29 11:23
 */
@SpringBootTest(classes = DemoApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class BaseCurServiceTest {

    @Autowired
    private BaseCurService baseCurService;

    @Autowired
    private ResourceLoader resourceLoader;


    private Properties properties = new Properties();

    @Test
    public void saveOrUpdateBtable() {
        Map<String, Object> requesMap = new HashMap<>();


        List<Map<String, Object>> columsList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            Map<String, Object> columMap = new HashMap<>();
            columMap.put("type", "String");
            columMap.put("key", "name" + i);
            columsList.add(columMap);
        }

        requesMap.put("tableName", "TestMate3");
        requesMap.put("colums", columsList);

        String param = JSONUtil.parseObj(requesMap).toString();
        baseCurService.saveOrUpdateBtable(param);
    }

    @Test
    public void testTwo() throws IOException {
        String localPath = System.getProperty("user.dir");
        String gitName = "19838212056";
        String gitPassWord = "zhang1996";
        Boolean aBoolean = JgitUtils.gitPull();
        if (aBoolean.equals(true)) {
            log.info("代码更新成功");
        } else {
            log.info("代码更新失败");
        }

    }

    @Test
    public void testThree(){
        String localPath = System.getProperty("user.dir");
        JgitUtils.gitCommit("update");
        Boolean push = JgitUtils.gitPush();
        if (push){
            log.info("推送成功");
        }else {
            log.error("推送失败");
        }
    }

    @Test
    public void testUser(){

        User build = User.builder()
                .passWord("sss")
                .userName("zhang")
                .date(new Date())
                .build();
        String s = JSONUtil.toJsonStr(build);

        System.out.println(s);
    }
}
