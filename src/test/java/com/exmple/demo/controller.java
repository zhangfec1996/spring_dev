package com.exmple.demo;

import com.example.demo.DemoApplication;
import com.example.demo.util.ClassFactoryUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zhangfc
 * @date 2021/12/29 9:04
 */
@SpringBootTest(classes = DemoApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class controller {

    @Test
    public void test01() {
        Set<Class<?>> classes = ClassFactoryUtils.getClasses("com.example");
        for (Class<?> aClass : classes) {
            System.out.println(aClass.getName());
        }
        List<String> nameList = classes.stream()
                .map(mclass -> mclass.getName())
                .collect(Collectors.toList());
        for (String s : nameList) {
            log.info(s);
        }
    }
}
