package com.example.demo.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

/**
 * @author zhangfc
 * @date 2022/1/15 9:54
 */

@Component
public class BzrLister implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        delegateTask.setAssignee("张欣欣");
    }
}
