package com.example.demo.management;

import com.example.demo.constant.ResponseMsg;
import com.example.demo.management.service.BaseCurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zhangfc
 * @date 2021/12/28 16:17
 */
@RestController
@RequestMapping("/management")
@Transactional(rollbackFor = Exception.class)
public class BaseCurController {

    @Autowired
    private BaseCurService baseCurService;


    /**
     * 保存或者更新表信息
     *
     * @param jsonString json串
     * @return ResponseMsg
     */
    @PostMapping("/saveOrUpdateBtable")
    public ResponseMsg saveOrUpdateBtable(@RequestBody String jsonString) {
        return baseCurService.saveOrUpdateBtable(jsonString);
    }


    @GetMapping("/getAllTable")
    public List<Map<String, Object>> getAllTable() {
        return baseCurService.getAllTable();
    }


    @PostMapping("/updateBtable")
    public ResponseMsg updateBtable(@RequestBody String jsonString) {
        int i = baseCurService.updateBtable(jsonString);
        return ResponseMsg.ok(i);
    }

}
