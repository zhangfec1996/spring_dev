package com.example.demo.controller;

import com.example.demo.constant.ResponseMsg;
import com.example.demo.service.ActivitiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

/**
 * ActivitiController类
 *
 * @author zhangfc
 * @date 2022/1/14 14:59
 * @see ActivitiService
 */
@Api(tags = "工作流相关操作")
@RestController
@RequestMapping("/activit")
public class ActivitiController {

    @Autowired
    private ActivitiService activitiService;

    /**
     * 流程部署
     *
     * @param jsonString {resourcename:文件名,
     *                   processname:流程name}
     * @return ResponseMsg
     */
    @ApiOperation("流程部署")
    @PostMapping("/processDeployment")
    @ApiImplicitParam(name = "jsonString",
            value = "{resourcename:'文件名',processname:'流程name'}",
            required = true,
            paramType = "body")
    public ResponseMsg processDeployment(@RequestBody String jsonString) {
        return activitiService.processDeployment(jsonString);
    }

    /**
     *         删除流程部署
     *
     * @param deploymentId 流程id
     * @param status       是否级联删除默认true
     * @return ResponseMsg
     */
    @ApiOperation("删除流程")
    @GetMapping("/delProcessDeployment")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deploymentId", value = "流程id", required = true, paramType = "query"),
            @ApiImplicitParam(name = "status", value = "是否级联删除谨慎使用 默认true", paramType = "query", dataType = "boolean")
    })
    public ResponseMsg deleteDeployment(@RequestParam("deploymentId") String deploymentId,
                                        @RequestParam(value = "status", defaultValue = "true", required = false) boolean status) {
        return activitiService.deleteDeployment(deploymentId, status);
    }

    /**
     * 删除流程实例和历史流程
     *
     * @param processInstanceId 流程实例id
     * @param deleteReason      删除原因
     * @return ResponseMsg
     */
    @GetMapping("/delProcessInstanceAndHis")
    @ApiOperation("删除流程实例和历史流程")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "processInstanceId", value = "流程实例id", required = true, paramType = "query"),
            @ApiImplicitParam(name = "deleteReason", value = "删除理由", paramType = "query")
    })
    public ResponseMsg delProceeInstanceAndHistory(@RequestParam("processInstanceId") String processInstanceId,
                                                   @RequestParam("deleteReason") String deleteReason) {
        return activitiService.delProceeInstanceAndHistory(processInstanceId, deleteReason);
    }


    /**
     * 启动流程实例
     *
     * @param processDefinitionKey 流程变量key
     * @param bussnessKey          业务主键key
     * @param jsonString           流程变量
     * @return ResponseMsg
     */
    @PostMapping("/startProcessInstance")
    @ApiOperation("启动流程实例")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "processDefinitionKey", value = "流程变量key", required = true, paramType = "query"),
            @ApiImplicitParam(name = "bussnessKey", value = "业务主键key", required = true, paramType = "query"),
            @ApiImplicitParam(name = "jsonString", value = "流程变量", dataTypeClass = String.class, paramType = "body", required = true)
    })
    public ResponseMsg startProcessInstance(@RequestParam("processDefinitionKey") String processDefinitionKey,
                                            @RequestParam("bussnessKey") String bussnessKey,
                                            @RequestBody String jsonString) {
        return activitiService.startProcessInstance(processDefinitionKey,
                bussnessKey,
                jsonString);
    }

    /**
     * 删除流程实例 不删除历史流程实例
     *
     * @param jsonString {processInstanceId:'实例id',
     *                   deleteReason:'删除理由'}
     * @return ResponseMsg
     */
    @ApiOperation("删除流程实例,不删除历史流程实例")
    @PostMapping("/deleteProcessInstance")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "jsonString", value = "{processInstanceId:'实例id',deleteReason:'删除理由'}", required = true, paramType = "body")
    })
    public ResponseMsg deleteProcessInstance(@RequestBody String jsonString) {
        return activitiService.deleteProcessInstance(jsonString);
    }


    /**
     * 根据业务主键key 和 流程实例获取动态流程图
     *
     * @param processInstanceId 流程实例id
     * @param bussniessKey      业务主键key
     * @throws IOException 异常处理
     */
    @GetMapping("/dynamicDiagram")
    @ApiOperation("获取动态流程图")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "processInstanceId", value = "流程实例id", required = true, paramType = "query"),
            @ApiImplicitParam(name = "bussniessKey", value = "业务主键key", required = true, paramType = "query")
    })
    public void dynamicDiagram(@RequestParam("processInstanceId") String processInstanceId,
                               @RequestParam("bussniessKey") String bussniessKey) throws IOException {
        activitiService.dynamicDiagram(processInstanceId, bussniessKey);
    }


    /**
     * 完成任务
     *
     * @param taskId    taskid
     * @param variables 流程变量
     * @return ResponseMsg
     */
    @PostMapping("/completeTask")
    @ApiOperation("完成任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", value = "任务id", required = true, paramType = "query"),
            @ApiImplicitParam(name = "variables", value = "流程变量", required = false, paramType = "body")
    })
    public ResponseMsg completeTask(@RequestParam("taskId") String taskId,
                                    @RequestBody(required = false) Map<String, Object> variables) {
        return activitiService.completeTask(taskId, variables);
    }


}
