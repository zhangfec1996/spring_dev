package com.example.demo.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.config.MyRequestHalper;
import com.example.demo.mapper.MenuMapper;
import com.example.demo.mapper.RoleMapper;
import com.example.demo.service.UserInfoService;
import com.example.demo.util.NoAuthorization;
import com.example.demo.vo.Menu;
import com.example.demo.vo.Role;
import com.example.demo.vo.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhangfc
 * @date 2021/12/17 13:47
 */
@RestController
@RequestMapping("/api")
public class LoginController {
    @Autowired
    private UserInfoService userInfoService;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private MenuMapper menuMapper;

    /**
     * 登录
     *
     * @param jsonString
     * @return
     */
    @NoAuthorization
    @PostMapping("/loginIn")
    public Map<String, Object> loginIn(@RequestBody String jsonString) {
        Map<String, Object> responseMap = new HashMap<>();
        Map<String, Object> map = JSONUtil.toBean(jsonString, Map.class);

        String username = Convert.toStr(map.get("user"));
        String password = Convert.toStr(map.get("pwd"));

//        if (StrUtil.equals(username, "admin")) {
//            if (StrUtil.equals(password, "123456")) {
//                reponseMap.put("token", EnumConstant.LoginConstant.L_TOKEN);
//                return reponseMap;
//            }
//        }
        String token = userInfoService.login(username, password);
        if (StrUtil.isNotEmpty(token)) {
            responseMap.put("token", token);
            return responseMap;
        } else {
            throw new RuntimeException("用户名或密码错误");
        }

    }

    /**
     * 注册
     *
     * @param jsonString
     * @return
     */
    @NoAuthorization
    @PostMapping("/register")
    public Map<String, Object> register(@RequestBody String jsonString) {

        Map<String, Object> reponseMap = new HashMap<>();
        Map<String, Object> map = JSONUtil.toBean(jsonString, Map.class);

        Boolean result = userInfoService.register(map);

        //System.out.println(username + "*******" + password);
        if (result) {
            reponseMap.put("msg", "注册成功");
            return reponseMap;
        }
        reponseMap.put("msg", "注册失败，请重试");
        return reponseMap;
    }

    /**
     * 获取左边栏
     *
     * @return Map
     */
    @GetMapping("/getmenu")
    public List<Map<String, Object>> getMenu() {
        UserInfo localUser = MyRequestHalper.getThreadLocal();
        String[] split = StrUtil.split(localUser.getRoleId(), ",");
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("roleid", split);
        List<Role> roleList = roleMapper.selectList(queryWrapper);


        String menuIds = CollUtil.join(roleList
                .stream()
                .map(Role::getMenuId)
                .collect(Collectors.toList()), ",");

        String[] menus = StrUtil.split(menuIds, ",");
        QueryWrapper<Menu> menuWrapper = new QueryWrapper<>();
        menuWrapper.in("menuid", menus);
        List<Menu> menuList = menuMapper.selectList(menuWrapper);

        List<Map<String, Object>> reponseList = menuList.stream().filter((menu) -> BeanUtil.isEmpty(menu.getFmenuid())).map((menu) -> {
            List<Map<String, Object>> mapList = menuMapper.listByFmenuid(menu.getMenuId());

            Map<String, Object> map = new HashMap<>();
            map.put("path", menu.getPath());
            map.put("name", menu.getName());
            map.put("label", menu.getLabel());
            map.put("icon", menu.getIcon());
            map.put("url", menu.getUrl());
            if (BeanUtil.isNotEmpty(mapList.get(0))){
                map.put("children", mapList);
            }
            return map;
        }).collect(Collectors.toList());
        return reponseList;
    }
}
