package com.example.demo.controller;

import com.dtflys.forest.Forest;
import com.example.demo.constant.ResponseMsg;
import com.example.demo.vo.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhangfc
 * @date 2022/3/29 14:57
 */
@RestController
@RequestMapping("/test")
public class TestController {

    private final RestTemplate restTemplate;


    public TestController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @GetMapping("/get")
    public void test01(HttpServletResponse response) {
        System.out.println("get");
        response.setStatus(200);
    }

    @GetMapping("/test02")
    public User test02() {

        return User.builder()
                .userName("zhangsa")
                .build();
    }

    @GetMapping("/test01")
    public ResponseMsg test01() {
        Map<String, Object> map = new HashMap<>();
        map.put("key1", "张三");
        map.put("key2", "李四");
        map.put("key3", "王五");
        int i = 1 / 0;
        return ResponseMsg.error(map);
    }

    @GetMapping("/testquery")
    public ResponseMsg testQuery(@RequestParam("name") String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("key1", "张三");
        map.put("key2", "李四");
        map.put("key3", "王五");
        return ResponseMsg.ok(map);
    }

    @GetMapping("/test03")
    public String test03() {
        Forest.get("http://localhost:8012/test/test01")
                .onSuccess((data, req, res) -> {
                    System.out.println(data);
                    System.out.println(req);
                    System.out.println(res);
                })
                .onError((ex, req, res) -> {
                    System.out.println(ex);
                    System.out.println(req);
                    System.out.println(res);
                })
                .execute();

        Forest.get("http://localhost:8012/test/testquery")
                .addQuery("name", "testQuery")
                .onSuccess((data, req, res) -> {
                    System.out.println(data);
                })
                .onError((ex, req, res) -> {
                    System.out.println(ex);
                }).execute();
        return "test03调用成功";
    }
}
