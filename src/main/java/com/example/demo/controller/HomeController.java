package com.example.demo.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.mapper.MenuMapper;
import com.example.demo.mapper.RoleMapper;
import com.example.demo.mapper.UserInfoMapper;
import com.example.demo.vo.Menu;
import com.example.demo.vo.Role;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhangfc
 * @date 2021/12/22 14:42
 */
@RestController
@RequestMapping("home")
public class HomeController {

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private MenuMapper menuMapper;

    @Resource
    private UserInfoMapper userInfoMapper;

    /**
     * 获取所有角色
     *
     * @return List<Map < String, Object>>
     */
    @GetMapping("/getrole")
    public List<Map<String, Object>> getRole() {

        List<Role> roleList = roleMapper.findAll();

        return roleList.stream().map((role) -> {
            String menuId = role.getMenuId();
            String[] split = StrUtil.split(menuId, ",");
            StringBuilder stringBuffer = new StringBuilder("");
            for (String s : split) {
                QueryWrapper<Menu> wrapper = new QueryWrapper<>();
                wrapper.eq("menuid", s);
                Menu menu = menuMapper.selectOne(wrapper);
                stringBuffer.append(menu.getLabel()).append(",");
            }
            Map<String, Object> map = new HashMap<>();
            map.put("roleid", role.getRoleId());
            map.put("rolename", role.getRoleName());
            map.put("menu", stringBuffer.toString());
            return map;
        }).collect(Collectors.toList());
    }

    @GetMapping("/roleByMenu")
    public List<Map<String, Object>> roleByMenu() {
        List<Menu> menus = menuMapper.selectAll();
        return menus.stream().map((menu) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("menuid", menu.getMenuId());
            map.put("name", menu.getLabel());
            return map;
        }).collect(Collectors.toList());
    }

    @PostMapping("/saverole")
    public Map<String, Object> saveRole(@RequestBody String jsonString) {
        Map<String, Object> map = JSONUtil.toBean(jsonString, Map.class);
        Map<String, Object> roleMap = JSONUtil.toBean(Convert.toStr(map.get("msg")), Map.class);

        List<?> menu = Convert.toList(roleMap.get("menu"));
        String name = Convert.toStr(roleMap.get("name"));
        String menus = CollUtil.join(menu, ",");
        Role role = Role.builder().roleName(name).menuId(menus).build();
        roleMapper.insert(role);

        Map<String, Object> reponseMap = new HashMap<>();
        reponseMap.put("msg", "创建成功");
        return reponseMap;
    }

    @PostMapping("/updateUserInfo")
    public Map<String, Object> updateUserInfo(@RequestBody String jsonString) {
        Map<String, Object> params = JSONUtil.toBean(jsonString, Map.class);
        Map<String, Object> msgMap = JSONUtil.toBean(Convert.toStr(params.get("msg")), Map.class);
        List<String> roleList = Convert.toList(String.class, msgMap.get("role"));
        String roles = CollUtil.join(roleList, ",");

        int row = userInfoMapper.updateUserRole(Convert.toInt(msgMap.get("userid")), roles);

        Map<String, Object> map = new HashMap<>();

        map.put("msg", "操作成功");

        return map;

    }
}
