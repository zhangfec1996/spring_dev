package com.example.demo.constant;

import java.io.File;

/**
 * @author zhangfc
 * @date 2021/12/17 13:57
 */
public class EnumConstant {
    public static class LoginConstant {
        public static String L_TOKEN = "jkssksjjjjjjj";
    }

    public static class UtilsConStant {

        /*
        file类型
         */
        public static String C_FILE = "file";

        /*
        jar类型
         */
        public static String C_JAR = "jar";
    }

    public static class filePath {

        public static String VO_PATH = File.separator + "src" +
                File.separator + "main" +
                File.separator + "java" +
                File.separator + "com" +
                File.separator + "example" +
                File.separator + "demo" +
                File.separator + "vo";

        public static String MAPPER_PATH = File.separator + "src" +
                File.separator + "main" +
                File.separator + "java" +
                File.separator + "com" +
                File.separator + "example" +
                File.separator + "demo" +
                File.separator + "mapper";
    }

    /**
     * git参数
     */
    public static class GitParam {

        /*
        帐号
         */
        public static String USERNAME = "19838212056";

        /*
        密码
         */
        public static String PASSWORD = "zhang1996";
    }
}
