package com.example.demo.constant;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangfc
 * @date 2021/12/23 13:56
 */
@ApiModel("响应参数")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class ResponseMsg {
    @ApiModelProperty("响应状态 0:成功  1或其他:失败")
    private int status;
    @ApiModelProperty("响应体数据")
    private Object msg;

    public static ResponseMsg ok() {
        return new ResponseMsg(0, "操作成功");
    }

    public static ResponseMsg ok(Object msg) {
        return new ResponseMsg(0, msg);
    }

    public static ResponseMsg error() {
        return new ResponseMsg(1, "失败");
    }

    public static ResponseMsg error(Object msg) {
        return new ResponseMsg(1, msg);
    }
}
