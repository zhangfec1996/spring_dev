package com.example.demo.constant;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;
@Getter
public enum YbksyfZtEnum {

    /**
     * 废弃
     */
    FQ("0", "废弃"),
    /**
     * 待备料
     */
    DBL("1", "采购备料"),
    /**
     * 待制版
     */
    DZB("2", "CAD制版"),
    /**
     * 待车版
     */
    DCB("3", "车版制作"),
    /**
     * 待确认
     */
    XSQR("4", "销售确认"),
    /**
     * 工艺物料
     */
    DYBZKS("5", "定义标准款式"),
    /**
     * 工艺工序
     */
    DYGYGX("6", "定义工艺工序"),
    /**
     * 已结束
     */
    YJS("9", "已结束");

    /**
     * 状态
     */
    private final String zt;
    /**
     * 状态对应的任务名称
     */
    private final String taskName;

    YbksyfZtEnum(String zt, String taskName) {
        this.zt = zt;
        this.taskName = taskName;
    }

    public static String getTaskName(String zt) {
        //
        final YbksyfZtEnum[] ybksyfZtEnums = YbksyfZtEnum.values();
        final Optional<YbksyfZtEnum> any =
                Arrays.stream(ybksyfZtEnums)
                        .filter(ybksyfZtEnum -> StrUtil.equals(ybksyfZtEnum.getZt(), zt))
                        .findAny();
        return any.map(YbksyfZtEnum::getTaskName).orElse(null);
    }
}