package com.example.demo.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * @author zhangfc
 * @date 2022/3/29 14:58
 */
@FeignClient(name = "test",value = "test",url = "http://localhost:8012")
public interface TestClient {

    @GetMapping("/test/get")
    public HttpServletResponse test01();
}
