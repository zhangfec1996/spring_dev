package com.example.demo.forest;

import com.dtflys.forest.annotation.Request;

/**
 * @author zhangfc
 * @date 2022/4/28 8:21
 */
public interface TestForestClient {

    /**
     * 测试案例
     *
     * @return String
     */
    @Request("http://localhost:8012/test/test01")
    String helloForest();
}
