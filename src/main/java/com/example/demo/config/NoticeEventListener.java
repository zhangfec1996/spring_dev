package com.example.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author zhangfc
 * @date 2022/1/24 13:49
 */
@Component
@Slf4j
public class NoticeEventListener implements ApplicationListener<NoticeEvent> {
    @Override
    @Async
    public void onApplicationEvent(NoticeEvent event) {
        log.info("事件开始执行......");
        String source = (String)event.getSource();
        log.info(source);
        log.info("事件执行结束......");
    }
}
