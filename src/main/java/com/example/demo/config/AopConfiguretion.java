package com.example.demo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author zhangfc
 * @date 2022/1/10 13:52
 */
@Configuration
@ComponentScan(value = "com.example.demo.controller")
@EnableAspectJAutoProxy // 开启AOP功能
public class AopConfiguretion {
}
