package com.example.demo.config;

import org.springframework.context.ApplicationEvent;

/**
 * @author zhangfc
 * @date 2022/1/24 13:48
 */
public class NoticeEvent extends ApplicationEvent {
    private String msg;
    /**
     * Create a new ApplicationEvent.
     *
     * @param msg the object on which the event initially occurred (never {@code null})
     */
    public NoticeEvent(String msg) {
        super(msg);
        this.msg = msg;
    }
}
