package com.example.demo.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private TokenInterceptor tokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册拦截器
        registry.addInterceptor(tokenInterceptor).addPathPatterns("/**")
                .excludePathPatterns("/**.html")
                .excludePathPatterns("/**/js/**")
                .excludePathPatterns("/**/css/**")
                .excludePathPatterns("/**/img/**")
                .excludePathPatterns("/**/favicon.ico")
                .excludePathPatterns("/**/fonts/**")
                .excludePathPatterns("/**/druid/**")
                .excludePathPatterns("/**.js")
                .excludePathPatterns("/**.css")
                .excludePathPatterns("/**/webjars/**")
                .excludePathPatterns("/**/swagger-resources/**")
                .excludePathPatterns("/error");

    }
    /**
     * 消息转换器配置[FastJson]
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        List<MediaType> jsonMediaTypes = new ArrayList<MediaType>();
        jsonMediaTypes.add(MediaType.APPLICATION_JSON);
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setCharset(StandardCharsets.UTF_8);
        fastJsonConfig.setSerializerFeatures(
                SerializerFeature.PrettyFormat,
                // 输出值为null的字段
                SerializerFeature.WriteMapNullValue,
                // 字符类型字段如果为null，输出为空字符串
                SerializerFeature.WriteNullStringAsEmpty,
                // 数值类型字段如果为null，输出为0
                SerializerFeature.WriteNullNumberAsZero,
                // 列表类型如果为null，输出[]
                SerializerFeature.WriteNullListAsEmpty,
                // 布尔类型字段如果为null，输出false
                SerializerFeature.WriteNullBooleanAsFalse,
                // 日期类型字段使用默认的日期格式[YYYY-MM-DD hh:mm:ss]
                SerializerFeature.WriteDateUseDateFormat,
                // 消除循环引用
                SerializerFeature.DisableCircularReferenceDetect
        );
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        fastConverter.setSupportedMediaTypes(jsonMediaTypes);
        fastConverter.setFastJsonConfig(fastJsonConfig);
        // 解决fastjson不生效的问题，因为SpringBoot默认将MappingJackson2HttpMessageConverter加入到了转换器中
        // 所以需要将MappingJackson2HttpMessageConverter转换成FastJsonHttpMessageConverter
        // SpringBoot消息转换器默认加载顺序
        // ByteArrayHttpMessageConverter
        // StringHttpMessageConverter
        // ResourceHttpMessageConverter
        // ResourceRegionHttpMessageConverter
        // SourceHttpMessageConverter
        // AllEncompassingFormHttpMessageConverter
        // MappingJackson2HttpMessageConverter
        // Jaxb2RootElementHttpMessageConverter
        for (int i = 0; i < converters.size(); i++) {
            if (converters.get(i) instanceof MappingJackson2HttpMessageConverter) {
                converters.set(i, fastConverter);
            }
        }
    }
}
