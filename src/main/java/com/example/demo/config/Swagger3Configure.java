package com.example.demo.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author zhangfc
 * @date 2022/1/14 15:35
 */
@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class Swagger3Configure {

    /**
     * 注入Docket, metadata
     *
     * @return Docket
     */
    @Bean
    public Docket docket() {
        //
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("流程部署")
                .apiInfo(metadataInfo())
                .select()
                // 指定生成文档的controller
                .apis(RequestHandlerSelectors.basePackage("com.example.demo"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo metadataInfo() {
        //
        return new ApiInfoBuilder()
                // 文档标题
                .title("Swagger3构建HD-METADATA Restful API")
                // 详细信息
                .description("对内提供接口")
                // 作者
                .contact(new Contact("zhangfc", "", ""))
                .version("1.0.0")
                .build();
    }

}
