package com.example.demo.config;

import com.example.demo.vo.UserInfo;

/**
 * @author zhangfc
 * @date 2021/12/23 10:20
 */
public class MyRequestHalper {
    private static ThreadLocal<UserInfo> threadLocal = new ThreadLocal<UserInfo>(){
        @Override
        protected UserInfo initialValue() {
            //这里设置初始值
            return super.initialValue();
        }
    };


    public static boolean setThreadLocal(UserInfo userInfo){
        try {
            threadLocal.set(userInfo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public static UserInfo getThreadLocal(){
        return threadLocal.get();
    }
}
