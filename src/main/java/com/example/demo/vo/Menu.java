package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

/**
 * @author zhangfc
 * @date 2021/12/22 15:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "menu")
@Builder(toBuilder = true)
public class Menu {

    @Id
    @TableField(value = "menuid")
    private Integer menuId;

    @TableField(value = "name")
    private String name;

    @TableField(value = "label")
    private String label;

    @TableField(value = "icon")
    private String icon;

    @TableField(value = "url")
    private String url;

    @TableField(value = "fmenuid")
    private String fmenuid;

    @TableField(value = "path")
    private String path;
}
