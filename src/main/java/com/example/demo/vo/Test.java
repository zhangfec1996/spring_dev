package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.*;


/**
 *@author zhangfc
 *@date Fri Dec 31 15:10:21 CST 2021
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Test")
@Builder(toBuilder = true)
public class Test {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer testId;


	private String testOne;


	private Integer testTwo;


	private Date testThree;


}