package com.example.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * @author zhangfc
 * @date 2021/12/24 11:19
 */
@Entity
@Table(name = "myOrder")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class MyOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orderId;

    private String oderNum;

    @DateTimeFormat(pattern = "yyyy-MM-dd") //向数据库存值时格式化
    private Date newOrder;

    private Long userId;

    private String userName;

    private Long mallId;

    private String mallName;

    private Long number;

    private String tel;

    private String myAdress;
}
