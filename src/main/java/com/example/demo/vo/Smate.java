package com.example.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


/**
 * @author zhangfc
 * @date Wed Dec 29 14:15:40 CST 2021
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Smate")
@Builder(toBuilder = true)
public class Smate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer smateId;

    private String itable;

    private String name;

    private String remark;

}