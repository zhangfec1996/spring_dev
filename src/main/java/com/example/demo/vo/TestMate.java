package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import java.util.*;


/**
 *@author zhangfc
 *@date Tue Jan 11 15:23:55 CST 2022
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "TestMate")
@Builder(toBuilder = true)
public class TestMate {


	@Id
	@TableId
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer testMateId;


	private String title;


	private String name;


}