package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

/**
 * @author zhangfc
 * @date 2021/12/22 15:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "role")
@Builder(toBuilder = true)
public class Role {
    @Id
    @TableField(value = "roleid")
    private Integer roleId;
    @TableField(value = "rolename")
    private String roleName;
    @TableField(value = "menuid")
    private String menuId;
}
