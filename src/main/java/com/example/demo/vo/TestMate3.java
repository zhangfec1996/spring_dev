package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import java.util.*;


/**
 *@author zhangfc
 *@date Tue Jan 11 10:39:15 CST 2022
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "TestMate3")
@Builder(toBuilder = true)
public class TestMate3 {


	@Id
	@TableId
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer testMate3Id;


	private String name0;


	private String name1;


	private String name2;


	private String name3;


	private String name4;


}