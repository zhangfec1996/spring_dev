package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import java.util.*;


/**
 *@author zhangfc
 *@date Fri Jan 07 15:25:06 CST 2022
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Sysvalue")
@Builder(toBuilder = true)
public class Sysvalue {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer sysvalueId;


	private String syskey;


	private String sysvalue;


	private String remark;


}