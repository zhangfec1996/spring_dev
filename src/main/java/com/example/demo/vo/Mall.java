package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * @author zhangfc
 * @date 2021/12/24 11:09
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "mall")
@Builder(toBuilder = true)
public class Mall {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//自增策略
    private Integer mallId;

    @DateTimeFormat(pattern = "yyyy-MM-dd") //向数据库存值时格式化
    private Date newDate;

    private String mallName;

    private String mallColor;

    private String weight;
}
