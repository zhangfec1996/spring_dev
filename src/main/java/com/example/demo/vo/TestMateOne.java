package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import java.util.*;


/**
 *@author zhangfc
 *@date Tue Jan 11 15:26:56 CST 2022
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "TestMateOne")
@Builder(toBuilder = true)
public class TestMateOne {


	@Id
	@TableId
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer testMateOneId;


	private String title;


	private String name;


}