package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "userinfo")
@Builder(toBuilder = true)
public class UserInfo {
    @Id
    @TableField(value = "userid")
    private Integer userId;
    @TableField(value = "username")
    private String userName;
    @TableField(value = "password")
    private String passWord;
    @TableField(value = "email")
    private String email;
    @TableField(value = "name")
    private String name;
    @TableField(value = "tel")
    private String tel;
    @TableField(value = "gender")
    private String gender;

    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @Temporal(TemporalType.TIMESTAMP)
    @TableField(value = "birthday")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") //向数据库存值时格式化
    private Date birthDay;
    @TableField(value = "roleid")
    private String roleId;
}
