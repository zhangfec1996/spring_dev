package com.example.demo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

/**
 * @author zhangfc
 * @date 2022/1/24 11:24
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class User {

    private String userName;

    private String passWord;

    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date date;
}
