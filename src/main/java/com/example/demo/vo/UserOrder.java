package com.example.demo.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * @author zhangfc
 * @date 2021/12/24 11:24
 */
@Entity
@Table(name = "userOrder")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class UserOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userorderId;

    private Integer userId;

    private String userName;

    private String oderNum;

    @DateTimeFormat(pattern = "yyyy-MM-dd") //向数据库存值时格式化
    private Date newDate;

    private String address;

    private Integer number;
}
