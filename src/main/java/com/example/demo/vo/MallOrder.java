package com.example.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * @author zhangfc
 * @date 2021/12/24 13:39
 */
@Entity
@Table(name = "mallOrder")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class MallOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer mallorderId;

    private Integer mallId;

    private Integer orderId;

    private String orderNum;

    private String mallName;

    private Date newDate;

    private Long number;
}
