package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.vo.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author zhangfc
 * @date 2021/12/22 15:19
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    @Select("select * from role")
    public List<Role> findAll();

    @Select("select * from role where roleid in (#{roleid})")
    public List<Role> listInRoleId(@Param("roleid") String roleid);

}
