package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.vo.MyOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author zhangfc
 * @date 2021/12/24 14:32
 */
@Mapper
public interface OrderMapper extends BaseMapper<MyOrder> {

    @Select("select * from my_order limit #{index},#{pageSize}")
    public List<MyOrder> listOrderByPage(@Param("index") int index,
                                         @Param("pageSize") int pageSize);

    @Select("select count(*) from my_order")
    public int sumOrder();
}
