package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.vo.MallOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author zhangfc
 * @date 2021/12/24 15:50
 */
@Mapper
public interface MallOrderMapper extends BaseMapper<MallOrder> {

    @Select("select * from mall_order limit #{index},#{pageSize}")
    public List<MallOrder> listMallOrderByPage(@Param("index") int index,
                                               @Param("pageSize") int pageSize);
    @Select("select count(*) from mall_order")
    public int sumTotal();
}
