package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.vo.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    @Select("select * from userinfo limit #{index},#{size}")
    public List<UserInfo> listByPage(@Param("index") int index, @Param("size") int size);

    @Select("select count(*) from userinfo")
    public int countByUseInfo();

    @Update("update userinfo set roleid = #{roles} where userid = #{userid}")
    public int updateUserRole(@Param("userid") int userId, @Param("roles") String roles);
}
