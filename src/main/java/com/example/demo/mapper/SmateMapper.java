package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.vo.Smate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author zhangfc
 * @date 2021/12/31 14:09
 */
@Mapper
public interface SmateMapper extends BaseMapper<Smate> {

    @Select("select * from smate")
    public List<Smate> listAllSmate();
}
