package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.example.demo.vo.*;
import org.apache.ibatis.annotations.*;


/**
 * @author zhangfc
 * @date 2022-01-11 15:23:55
 */
@Mapper
public interface TestMateMapper extends BaseMapper<TestMate> {


}