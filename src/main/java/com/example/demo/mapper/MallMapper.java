package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.vo.Mall;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author zhangfc
 * @date 2021/12/24 13:45
 */
@Mapper
public interface MallMapper extends BaseMapper<Mall> {

    @Select("select * from mall limit #{index},#{size}")
    public List<Mall> listMallByPage(@Param("index") int index,@Param("size") int size);

    @Select("select count(*) from mall")
    public int sumMall();

}
