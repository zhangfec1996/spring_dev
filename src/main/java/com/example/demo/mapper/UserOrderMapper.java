package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.vo.UserOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangfc
 * @date 2021/12/24 15:51
 */
@Mapper
public interface UserOrderMapper extends BaseMapper<UserOrder> {

    @Select("select * from user_order limit #{index},#{pageSize}")
    public List<UserOrder> listUsrOdrByPage(@Param("index") int index,
                                            @Param("pageSize") int pageSize);


    @Select("select count(*) from user_order")
    public int sumTotal();

}
