package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.vo.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author zhangfc
 * @date 2021/12/22 15:20
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
    @Select("select * from menu")
    public List<Menu> selectAll();

    @Select("select m2.name,m2.label,m2.icon,m2.url,m2.path from menu m1 left join menu m2 on m1.menuid = m2.fmenuid where m1.menuid = #{menuid}")
    public List<Map<String,Object>> listByFmenuid(@Param("menuid") int menuid);
}
