package com.example.demo.handler;

import com.example.demo.constant.ResponseMsg;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author zhangfc
 * @date 2021/12/23 14:01
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseMsg exceptionHandler(Exception e) {
        e.printStackTrace();
        return ResponseMsg.error(e.getMessage());
    }
}
