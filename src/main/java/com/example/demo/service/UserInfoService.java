package com.example.demo.service;

import com.example.demo.vo.UserInfo;

import java.util.Map;

public interface UserInfoService {
    Boolean register(Map map);

    String login(String username, String password);

    UserInfo queryUserByToken(String token);
}
