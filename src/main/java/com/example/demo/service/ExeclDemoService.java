package com.example.demo.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zhangfc
 * @date 2022/2/17 14:21
 */
@Component
public class ExeclDemoService {
    private final static Lock reentrantLock = new ReentrantLock(true);
    @Async
    public void sout(int i){
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("异步方法执行"+i);
    }
}
