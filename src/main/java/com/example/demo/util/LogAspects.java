package com.example.demo.util;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author zhangfc
 * @date 2022/1/10 13:54
 */
@Aspect
@Component
@Slf4j
public class LogAspects {

    @Pointcut("execution(* com.example.demo.controller.*.*(..))")
    public void pointCut() {
    }

    @Before("pointCut()")
    public void logStart(JoinPoint joinPoint) {
        //获取请求参数列表
        Object[] args = joinPoint.getArgs();
        //获取请求方法
        String name = joinPoint.getSignature().getName();
        log.info("---当前方法为:" + name + "======参数列表" + Arrays.asList(args));
    }

    @After("pointCut()") //后置通知
    public void logAfter(JoinPoint joinPoint) {
        String name = joinPoint.getSignature().getName();
//        log.info("@After---" + name + "方法结束......");
    }

    @AfterReturning(value = "pointCut()",returning = "result")
    public void logReturn(Object result){
        log.info("---正常返回---运行结果是:{}",result);
    }

    @AfterThrowing(value = "pointCut()",throwing = "exception")
    public void logException(Exception exception){
        log.error("---运行异常---异常信息是:{}",exception.getMessage());
    }

    @Around("pointCut()") //环绕通知
    public Object Around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
//        log.info("@Arount:执行目标方法之前...");
        //相当于开始调用div
        Object proceed = proceedingJoinPoint.proceed();
//        log.info("@Arount:执行目标方法之后...");
        return proceed;
}
}
