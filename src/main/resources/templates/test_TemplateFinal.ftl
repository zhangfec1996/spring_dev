<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
          xmlns:o="urn:schemas-microsoft-com:office:office"
          xmlns:x="urn:schemas-microsoft-com:office:excel"
          xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
          xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
          xmlns:html="http://www.w3.org/TR/REC-html40">
    <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
        <Author>${auther}</Author>
        <LastAuthor>${auther}</LastAuthor>
        <Revision>1</Revision>
        <Created>${today?string("yyyy年MM月dd日")}</Created>
        <Version>11.5606</Version>
    </DocumentProperties>
    <CustomDocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
        <Generator dt:dt="string">NPOI</Generator>
        <Generator_x0020_Version dt:dt="string">2.1.3</Generator_x0020_Version>
        <KSOProductBuildVer dt:dt="string">2052-10.8.0.5715</KSOProductBuildVer>
    </CustomDocumentProperties>
    <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
        <WindowHeight>10368</WindowHeight>
        <WindowWidth>20880</WindowWidth>
        <WindowTopX>0</WindowTopX>
        <WindowTopY>0</WindowTopY>
        <ProtectStructure>False</ProtectStructure>
        <ProtectWindows>False</ProtectWindows>
    </ExcelWorkbook>
    <Styles>
        <Style ss:ID="Default" ss:Name="Normal">
            <Alignment ss:Vertical="Bottom"/>
            <Borders/>
            <Font x:Family="Swiss"/>
            <Interior/>
            <NumberFormat/>
            <Protection/>
        </Style>
        <Style ss:ID="m196574646">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
            <Borders>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
        </Style>
        <Style ss:ID="m196574656">
            <Borders>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9" ss:Bold="1"/>
        </Style>
        <Style ss:ID="s64">
            <Font x:Family="Swiss" ss:Size="9"/>
        </Style>
        <Style ss:ID="s65">
            <Font x:Family="Swiss" ss:Size="9" ss:Bold="1"/>
        </Style>
        <Style ss:ID="s66">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"
                       ss:WrapText="1"/>
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
        </Style>
        <Style ss:ID="s67">
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9" ss:Bold="1"/>
        </Style>
        <Style ss:ID="s68">
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
        </Style>
        <Style ss:ID="s69">
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
            <Interior ss:Color="#FFFF00" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s70">
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
            <Interior ss:Color="#33CCCC" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s71">
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
            <Interior ss:Color="#99CC00" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s72">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
        </Style>
        <Style ss:ID="s73">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font ss:FontName="宋体" x:CharSet="134" ss:Size="9"/>
        </Style>
        <Style ss:ID="s74">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
            <Interior ss:Color="#FFFF00" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s81">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
            <Font ss:FontName="宋体" x:CharSet="134" ss:Size="18" ss:Bold="1"/>
        </Style>
        <Style ss:ID="s83">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
        </Style>
        <Style ss:ID="s86">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
        </Style>
        <Style ss:ID="s87">
            <Alignment ss:Horizontal="Center" ss:Vertical="Distributed"/>
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
        </Style>
        <Style ss:ID="s89">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
            <Interior ss:Color="#FFFF00" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s90">
            <Alignment ss:Vertical="Top"/>
            <Borders>
                <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
            </Borders>
            <Font x:Family="Swiss" ss:Size="9"/>
        </Style>
    </Styles>
    <Worksheet ss:Name="sheet1">
        <!--此处用于计算主辅面料的总条数-->
        <#assign total=0>
        <#list countList as param>
            <#list param.paramList as items>
                <#assign total = total+items.tableList?size+2>
            </#list>
        </#list>
        <!--此处用于计算所有面料共多少行-->
        <#assign total = total + countList?size>
        <Table ss:ExpandedColumnCount="14" ss:ExpandedRowCount="${total+30}" x:FullColumns="1"
               x:FullRows="1" ss:DefaultColumnWidth="49.2"
               ss:DefaultRowHeight="200.10000000000002">
            <Column ss:AutoFitWidth="0" ss:Width="57"/>
            <Column ss:AutoFitWidth="0" ss:Width="99.6"/>
            <Column ss:AutoFitWidth="0" ss:Width="75.600000000000009"/>
            <Column ss:AutoFitWidth="0" ss:Width="57"/>
            <Column ss:AutoFitWidth="0" ss:Width="48"/>
            <Column ss:AutoFitWidth="0" ss:Width="40.8"/>
            <Column ss:AutoFitWidth="0" ss:Width="63"/>
            <Column ss:AutoFitWidth="0" ss:Width="48" ss:Span="4"/>
            <Column ss:Index="13" ss:AutoFitWidth="0" ss:Width="60"/>
            <Column ss:AutoFitWidth="0" ss:Width="66.600000000000009"/>
            <Row ss:AutoFitHeight="0" ss:Height="24.9">
                <Cell ss:MergeAcross="13" ss:StyleID="s81">
                    <Data ss:Type="String">产品核价单</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="17.099999999999998">
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String">样品名称：</Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String">VIP Med</Data>
                </Cell>
                <Cell ss:Index="6" ss:StyleID="s64">
                    <Data ss:Type="String">样品代码：</Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String">HAM-04-2201-buf</Data>
                </Cell>
                <Cell ss:Index="11" ss:StyleID="s64">
                    <Data ss:Type="String">BOM编号:</Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String">MO21090726</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="17.099999999999998">
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String">客户：</Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String">HAMMITT</Data>
                </Cell>
                <Cell ss:Index="6" ss:StyleID="s64">
                    <Data ss:Type="String">色别：</Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String">香草白</Data>
                </Cell>
                <Cell ss:Index="11" ss:StyleID="s64">
                    <Data ss:Type="String">制表日期:</Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String">${today?string("yyyy年MM月dd日")}</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="18">
                <Cell ss:StyleID="s65">
                    <Data ss:Type="String">1、核准产品用料明细：(可直接移作《产品用料定额》)</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="20.1">
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">项目</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">材料编号</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">材料/部位名称</Data>
                </Cell>
                <Cell ss:MergeAcross="1" ss:StyleID="m196574646">
                    <Data ss:Type="String">单件规格</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">件数</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">净合数(规格单耗)</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">损耗率</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">折合数(用料定额)</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">单价\元</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">报价合计</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">材料税率</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">工厂测算用材料定额成本</Data>
                </Cell>
                <Cell ss:MergeDown="1" ss:StyleID="s83">
                    <Data ss:Type="String">材料供应商</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="20.1">
                <Cell ss:Index="4" ss:StyleID="s66">
                    <Data ss:Type="String">长</Data>
                </Cell>
                <Cell ss:StyleID="s66">
                    <Data ss:Type="String">宽</Data>
                </Cell>
            </Row>
            <!--定义主要材料的条数-->
            <#assign maincailiao = 0>
            <!--定义辅助材料的条数-->
            <#assign fuzhucailiao = 0>
            <!--定义五金配件的条数-->
            <#assign wujinpeijian = 0>
            <!--定义包装材料的条数-->
            <#assign baozhuangcailiao = 0>
            <!--遍历最大list-->
            <#list countList as params>
            <!--记录每个项目的总条数用于为项目进行单元格的合并-->
                <#assign evertotal = 0>
                <#list params.paramList as param>
                    <#assign evertotal = evertotal + param.tableList?size +2>
                </#list>
                <#if params_index == 0>
                    <#assign maincailiao = evertotal +1>
                <#elseif params_index == 1>
                    <#assign fuzhucailiao = evertotal + 1>
                <#elseif params_index == 2>
                    <#assign wujinpeijian = evertotal + 1>
                <#elseif params_index == 3>
                    <#assign baozhuangcailiao = evertotal + 1>
                </#if>
            <Row ss:AutoFitHeight="0" ss:Height="18">
                <Cell ss:MergeDown="${evertotal}" ss:StyleID="s87">
                    <Data ss:Type="String">${params.name}</Data>
                </Cell>
                <Cell ss:StyleID="s67">
                    <Data ss:Type="String">${params.paramList[0].name}</Data>
                </Cell>
            </Row>
            <!--用于计算合计处向上累加的条数-->
                <#assign sumtotal = 0>
                <#list params.paramList as tableParam>
                    <#if tableParam_index gt 0>
                    <Row ss:AutoFitHeight="0" ss:Height="18">
                        <Cell ss:StyleID="s67">
                            <Data ss:Type="String">${tableParam.name}</Data>
                        </Cell>
                    </Row>
                    </#if>
                    <#assign tabletotal = 0>
                    <#list tableParam.tableList as item>
                        <#assign sumtotal = sumtotal + 1>
                        <#assign tabletotal = tabletotal + 1>
                    <Row ss:AutoFitHeight="0" ss:Height="18">
                        <Cell ss:Index="2" ss:StyleID="s68">
                            <Data ss:Type="String">${item.num}</Data>
                        </Cell>
                        <Cell ss:StyleID="s68">
                            <Data ss:Type="String">${item.name}</Data>
                        </Cell>
                        <Cell ss:StyleID="s68">
                            <Data ss:Type="Number">${item.chang}</Data>
                        </Cell>
                        <Cell ss:StyleID="s68">
                            <Data ss:Type="Number">${item.kuan}</Data>
                        </Cell>
                        <Cell ss:StyleID="s68">
                            <Data ss:Type="String">${item.js}</Data>
                        </Cell>
                        <Cell ss:StyleID="s68"
                              ss:Formula="${item.equation}">
                            <Data ss:Type="Number">1.1409734000000001E-2</Data>
                        </Cell>
                        <Cell ss:StyleID="s68">
                            <Data ss:Type="String"></Data>
                        </Cell>
                        <Cell ss:StyleID="s68">
                            <Data ss:Type="String"></Data>
                        </Cell>
                        <Cell ss:StyleID="s70">
                            <Data ss:Type="String"></Data>
                        </Cell>
                        <Cell ss:StyleID="s68">
                            <Data ss:Type="String"></Data>
                        </Cell>
                        <Cell ss:StyleID="s71">
                            <Data ss:Type="String"></Data>
                        </Cell>
                        <Cell ss:StyleID="s68">
                            <Data ss:Type="String"></Data>
                        </Cell>
                        <Cell ss:StyleID="s68">
                            <Data ss:Type="String">${item.compony}</Data>
                        </Cell>
                    </Row>
                    </#list>
            <Row ss:AutoFitHeight="0" ss:Height="18">
                <Cell ss:Index="2" ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(SUM(R[-${tabletotal}]C:R[-1]C),3)">
                    <Data ss:Type="Number">0.05</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="Number">32</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(RC[-2]*(1+RC[-1]*0.01),3)">
                    <Data ss:Type="Number">6.6000000000000003E-2</Data>
                </Cell>
                <Cell ss:StyleID="s70" ss:Formula="=2.8*6.4">
                    <Data ss:Type="Number">17.919999999999998</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(RC[-2]*RC[-1],2)">
                    <Data ss:Type="Number">1.18</Data>
                </Cell>
                <Cell ss:StyleID="s71">
                    <Data ss:Type="Number">0</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(RC[-3]/(1+RC[-1]*0.01),2)">
                    <Data ss:Type="Number">1.18</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
            </Row>
                </#list>
            <Row ss:AutoFitHeight="0" ss:Height="18">
                <Cell ss:Index="2" ss:StyleID="s69">
                    <Data ss:Type="String">合计：</Data>
                </Cell>
                <Cell ss:StyleID="s69">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s69">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s69">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s69">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s69">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s69">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s69" ss:Formula="=ROUND(SUM(R[-${sumtotal+(params.paramList?size*2)}]C:R[-1]C),3)">
                    <Data ss:Type="Number">14.2</Data>
                </Cell>
                <Cell ss:StyleID="s69">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s69" ss:Formula="=ROUND(SUM(R[-${sumtotal+(params.paramList?size*2)}]C:R[-1]C),2)">
                    <Data ss:Type="Number">18.63</Data>
                </Cell>
                <Cell ss:StyleID="s69">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s69" ss:Formula="=ROUND(SUM(R[-${sumtotal+(params.paramList?size*2)}]C:R[-1]C),2)">
                    <Data ss:Type="Number">17.04</Data>
                </Cell>
                <Cell ss:StyleID="s69">
                    <Data ss:Type="String"></Data>
                </Cell>
            </Row>
            </#list>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s67">
                    <Data ss:Type="String">2、料、工、费率计算：(可据此汇总下达《内部订单》，本栏自行计算）</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">项目</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">主要材料</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">辅助材料</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">五金配件</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">包装材料</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">材料成本合计</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">工资</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">制造费用</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">综合费用</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">产品成本合计</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">金额</Data>
                </Cell>
                <#if maincailiao == 0>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="Number">0</Data>
                </Cell>
                <#else>
                <Cell ss:StyleID="s68" ss:Formula="=R[-${total - maincailiao + 4}]C[9]">
                    <Data ss:Type="Number">102.68</Data>
                </Cell>
                </#if>
                <#if fuzhucailiao == 0>
                     <Cell ss:StyleID="s68">
                         <Data ss:Type="Number">0</Data>
                     </Cell>
                <#else>
                     <Cell ss:StyleID="s68" ss:Formula="=R[-${total - maincailiao - fuzhucailiao + 4}]C[8]">
                         <Data ss:Type="Number">2.94</Data>
                     </Cell>
                </#if>
                <#if wujinpeijian == 0>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="Number">0</Data>
                </Cell>
                <#else>
                <Cell ss:StyleID="s68" ss:Formula="=R[-${total - maincailiao - fuzhucailiao - wujinpeijian + 4}]C[7]">
                    <Data ss:Type="Number">57.89</Data>
                </Cell>
                </#if>
                <#if baozhuangcailiao == 0>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="Number">0</Data>
                </Cell>
                <#else>
                <Cell ss:StyleID="s68" ss:Formula="=R[-4]C[6]">
                    <Data ss:Type="Number">18.63</Data>
                </Cell>
                </#if>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(SUM(RC[-5]:RC[-2]),2)">
                    <Data ss:Type="Number">182.14</Data>
                </Cell>
                <Cell ss:StyleID="s68"
                      ss:Formula="=ROUND(R[3]C[-1]/R[3]C[4]*(1+R[4]C[-1]*0.01)*(1+R[5]C[-1]*0.01),2)">
                    <Data ss:Type="Number">35.38</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(RC[-1]*R[4]C[3]*0.01,2)">
                    <Data ss:Type="Number">8.18</Data>
                </Cell>
                <Cell ss:StyleID="s68"
                      ss:Formula="=ROUND((RC[-3]+RC[-2]+RC[-1])/(1-R[5]C[-8]*0.01-R[5]C[2]*0.01)*R[5]C[2]*0.01,2)">
                    <Data ss:Type="Number">18.72</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(SUM(RC[-6]:RC[-2]),2)">
                    <Data ss:Type="Number">244.42</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String">售价成本率%</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(R[-1]C/(R[4]C[1]*0.01),2)">
                    <Data ss:Type="Number">42.01</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(R[-1]C/(R[4]C*0.01),2)">
                    <Data ss:Type="Number">1.2</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(R[-1]C/(R[4]C[-1]*0.01),2)">
                    <Data ss:Type="Number">23.68</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(R[-1]C/(R[4]C[-2]*0.01),2)">
                    <Data ss:Type="Number">7.62</Data>
                </Cell>
                <Cell ss:Index="7" ss:StyleID="s68"
                      ss:Formula="=ROUND(R[-1]C/(R[4]C[-4]*0.01),2)">
                    <Data ss:Type="Number">74.52</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(R[-1]C/(R[4]C[-5]*0.01),2)">
                    <Data ss:Type="Number">14.48</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(R[-1]C/(R[4]C[-6]*0.01),2)">
                    <Data ss:Type="Number">3.35</Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(R[-1]C/(R[4]C[-7]*0.01),2)">
                    <Data ss:Type="Number">7.66</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s68" ss:Formula="=ROUND(R[-1]C/(R[4]C[-10]*0.01),2)">
                    <Data ss:Type="Number">100</Data>
                </Cell>
                <Cell ss:StyleID="s68">
                    <Data ss:Type="String"></Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:MergeAcross="5" ss:StyleID="m196574656">
                    <Data ss:Type="String">3、核准公司价：（随附事前确认的核价用相关资料）</Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:MergeAcross="2" ss:StyleID="s64">
                    <Data ss:Type="String">全定额：4.85成件定额：6.2</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:MergeAcross="1" ss:StyleID="s86">
                    <Data ss:Type="String">利润</Data>
                </Cell>
                <Cell ss:MergeAcross="1" ss:StyleID="s86">
                    <Data ss:Type="String">内部基价</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">标准工价￥</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="Number">110</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">汇率%</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="Number">6.4</Data>
                </Cell>
                <Cell ss:MergeAcross="1" ss:StyleID="s86">
                    <Data ss:Type="String">生产定额：工/只</Data>
                </Cell>
                <Cell ss:MergeAcross="2" ss:StyleID="s86">
                    <Data ss:Type="Number">4.85</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">金额￥</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">利润率%</Data>
                </Cell>
                <Cell ss:MergeAcross="1" ss:StyleID="s86">
                    <Data ss:Type="String">产品成本合计/(1-利润率)</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">附加工资%</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s74">
                    <Data ss:Type="String">30</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:MergeAcross="1" ss:StyleID="s86">
                    <Data ss:Type="String">工资制造费用率%</Data>
                </Cell>
                <Cell ss:MergeAcross="2" ss:StyleID="s89">
                    <Data ss:Type="String">23.11</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s72" ss:Formula="=ROUND(RC[2]*RC[1]*0.01,2)">
                    <Data ss:Type="Number">0</Data>
                </Cell>
                <Cell ss:StyleID="s72"/>
                <Cell ss:MergeAcross="1" ss:StyleID="s86"
                      ss:Formula="=ROUND(R[-5]C[10]/(1-RC[-1]*0.01),2)">
                    <Data ss:Type="Number">244.42</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">加班工资率%</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="Number">20</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">折美元售价</Data>
                </Cell>
                <Cell ss:StyleID="s72" ss:Formula="=ROUND(RC[-6]/R[-2]C,2)">
                    <Data ss:Type="Number">38.19</Data>
                </Cell>
                <Cell ss:MergeAcross="1" ss:StyleID="s86">
                    <Data ss:Type="String">售价综合费率%</Data>
                </Cell>
                <Cell ss:MergeAcross="2" ss:StyleID="s86">
                    <Data ss:Type="Number">7.66</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s67">
                    <Data ss:Type="String">4、MGI 对外售价计算:</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:MergeAcross="1" ss:StyleID="s86">
                    <Data ss:Type="String">利润</Data>
                </Cell>
                <Cell ss:MergeAcross="1" ss:StyleID="s86">
                    <Data ss:Type="String">海外销售佣金</Data>
                </Cell>
                <Cell ss:MergeAcross="2" ss:MergeDown="1" ss:StyleID="s86">
                    <Data ss:Type="String">对外最终售价￥</Data>
                </Cell>
                <Cell ss:MergeAcross="1" ss:MergeDown="1" ss:StyleID="s86">
                    <Data ss:Type="String">对外最终售价＄</Data>
                </Cell>
                <Cell ss:MergeAcross="4" ss:MergeDown="2" ss:StyleID="s90">
                    <Data ss:Type="String">备注:</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">金额￥</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">利润率%</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">金额￥</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">佣金率%</Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s72"
                      ss:Formula="=ROUND(R[-4]C[2]/(1-RC[1]*0.01-RC[3]*0.01)*RC[1]*0.01,2)">
                    <Data ss:Type="Number">0</Data>
                </Cell>
                <Cell ss:StyleID="s72"/>
                <Cell ss:StyleID="s72"
                      ss:Formula="=ROUND(R[-4]C/(1-RC[-1]*0.01-RC[1]*0.01)*RC[1]*0.01,2)">
                    <Data ss:Type="Number">0</Data>
                </Cell>
                <Cell ss:StyleID="s72"/>
                <Cell ss:MergeAcross="2" ss:StyleID="s86"
                      ss:Formula="=ROUND(R[-4]C[-2]+RC[-4]+RC[-2],2)">
                    <Data ss:Type="Number">244.42</Data>
                </Cell>
                <Cell ss:MergeAcross="1" ss:StyleID="s86"
                      ss:Formula="=ROUND(RC[-3]/R[-6]C[1],2)">
                    <Data ss:Type="Number">38.19</Data>
                </Cell>
            </Row>
            <Row ss:Height="13.2">
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">出格：</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">封样：</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">制表：</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">复核：</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">审核批准：</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s64">
                    <Data ss:Type="String"></Data>
                </Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="24">
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String">备注：</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="Number">1</Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s72">
                    <Data ss:Type="String"></Data>
                </Cell>
                <Cell ss:StyleID="s73">
                    <Data ss:Type="String">最终价格</Data>
                </Cell>
                <Cell ss:StyleID="s72" ss:Formula="=R[-3]C[-6]+RC[-4]">
                    <Data ss:Type="Number">39.19</Data>
                </Cell>
            </Row>
        </Table>
        <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
            <Unsynced/>
            <NoSummaryRowsBelowDetail/>
            <NoSummaryColumnsRightDetail/>
            <DisplayPageBreak/>
            <FitToPage/>
            <Print>
                <FitHeight>0</FitHeight>
                <ValidPrinterInfo/>
                <HorizontalResolution>300</HorizontalResolution>
                <VerticalResolution>300</VerticalResolution>
            </Print>
            <PageBreakZoom>60</PageBreakZoom>
            <Selected/>
            <Panes>
                <Pane>
                    <Number>3</Number>
                    <ActiveRow>42</ActiveRow>
                    <ActiveCol>17</ActiveCol>
                </Pane>
            </Panes>
            <ProtectObjects>False</ProtectObjects>
            <ProtectScenarios>False</ProtectScenarios>
        </WorksheetOptions>
    </Worksheet>
</Workbook>
